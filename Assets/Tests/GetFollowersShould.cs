using System.Collections;
using Etermax.UnityReactiveX.Testing;
using NSubstitute;
using NUnit.Framework;
using Services.Network;
using UniRx;
using UnityEngine;
using UnityEngine.TestTools;
using Users.Actions;
using Users.Data;

namespace Tests
{
    public class GetFollowsShould
    {
        [TestCase("name")]
        [TestCase("")]
        public void GetFollowersOfSuccessfully(string nickName)
        {
            var networkService = Substitute.For<INetworkService>();
            var apiResponse = new Subject<string>();
            
            var command = "getFollowerOf";
            networkService.PostJsonData(command, nickName).Returns(apiResponse);

            var action = new GetFollowsAction(networkService);
            var observable = action.GetFollowersOf(nickName).Check();
            apiResponse.OnNext(nickName);
            apiResponse.OnCompleted();
            
            observable.EmittedOnce();
            networkService.Received(1).PostJsonData(command,
                Arg.Is(nickName));
        }
        
        [TestCase("name")]
        [TestCase("")]
        public void GetFollowedBySuccessfully(string nickName)
        {
            var networkService = Substitute.For<INetworkService>();
            var apiResponse = new Subject<string>();
            
            var command = "getFollowedBy";
            networkService.PostJsonData(command, nickName).Returns(apiResponse);

            var action = new GetFollowsAction(networkService);
            var observable = action.GetFollowedBy(nickName).Check();
            apiResponse.OnNext(nickName);
            apiResponse.OnCompleted();
            
            observable.EmittedOnce();
            networkService.Received(1).PostJsonData(command,
                Arg.Is(nickName));
        }
    }
}