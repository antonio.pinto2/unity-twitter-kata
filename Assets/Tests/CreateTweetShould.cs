using System.Collections;
using Etermax.UnityReactiveX.Testing;
using NSubstitute;
using NUnit.Framework;
using Services.Network;
using Tweets.actions;
using Tweets.data;
using UniRx;
using UnityEngine;
using UnityEngine.TestTools;
using Users.Actions;
using Users.Data;

namespace Tests
{
    public class CreateTweetShould
    {
        // A Test behaves as an ordinary method
        [TestCase("user1", "hello!")]
        [TestCase("user2", "")]
        [TestCase("user1", "hello world")]
        [TestCase("", "")]
        public void CreateTweetSuccessfully(string nickName,string text)
        {
            var tweet = JsonUtility.ToJson(new Tweet(nickName, text));

            var networkService = Substitute.For<INetworkService>();
            var apiResponse = new Subject<string>();


            var command = "createTweet";
            networkService.PostJsonData(command, tweet).Returns(apiResponse);

            var action = new CreateTweetAction(networkService);
            var followObservable = action.Tweet(nickName, text).Check();
            apiResponse.OnNext(tweet);
            apiResponse.OnCompleted();
            
            followObservable.EmittedOnce();
            networkService.Received(1).PostJsonData(command,
                Arg.Is(tweet));
        }
    }
}