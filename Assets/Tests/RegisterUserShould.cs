using System.Collections;
using Etermax.UnityReactiveX.Testing;
using NSubstitute;
using NUnit.Framework;
using Services.Network;
using UniRx;
using UnityEngine;
using UnityEngine.TestTools;
using Users.Actions;
using Users.Data;

namespace Tests
{
    public class RegisterUserShould
    {
        // A Test behaves as an ordinary method
        [TestCase("realname", "nickname")]
        [TestCase("nickname", "nickname")]
        [TestCase("realname", "")]
        [TestCase("", "nickname")]
        [TestCase("", "")]
        public void RegisterUserSuccessfully(string realName,string nickName)
        {
            var user = JsonUtility.ToJson(new User(realName, nickName));

            var networkService = Substitute.For<INetworkService>();
            var apiResponse = new Subject<string>();
            
            var command = "registerUser";
            networkService.PostJsonData(command, user).Returns(apiResponse);

            var userApi = new RegisterUserAction(networkService);
            var registerUserObservable = userApi.RegisterUser(realName, nickName).Check();
            apiResponse.OnNext(user);
            apiResponse.OnCompleted();
            
            registerUserObservable.EmittedOnce();
            networkService.Received(1).PostJsonData(command,
                Arg.Is(user));
        }
    }
}