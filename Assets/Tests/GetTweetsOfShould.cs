using System.Collections;
using Etermax.UnityReactiveX.Testing;
using NSubstitute;
using NUnit.Framework;
using Services.Network;
using Tweets.actions;
using UniRx;
using UnityEngine;
using UnityEngine.TestTools;
using Users.Actions;
using Users.Data;

namespace Tests
{
    public class GetTweetsOfShould
    {
        [TestCase("name")]
        [TestCase("")]
        public void GetTweetsSuccessfully(string nickName)
        {
            var networkService = Substitute.For<INetworkService>();
            var apiResponse = new Subject<string>();
            
            var command = "getTweetsOf";
            networkService.PostJsonData(command, nickName).Returns(apiResponse);

            var action = new GetTweetsOfAction(networkService);
            var observable = action.GetTweetsOf(nickName).Check();
            apiResponse.OnNext(nickName);
            apiResponse.OnCompleted();
            
            observable.EmittedOnce();
            networkService.Received(1).PostJsonData(command,
                Arg.Is(nickName));
        }
    }
}