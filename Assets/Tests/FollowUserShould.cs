using System.Collections;
using Etermax.UnityReactiveX.Testing;
using NSubstitute;
using NUnit.Framework;
using Services.Network;
using UniRx;
using UnityEngine;
using UnityEngine.TestTools;
using Users.Actions;
using Users.Data;

namespace Tests
{
    public class FollowUserShould
    {
        // A Test behaves as an ordinary method
        [TestCase("user1", "user2")]
        [TestCase("user2", "user1")]
        [TestCase("user1", "")]
        [TestCase("", "user1")]
        [TestCase("", "")]
        public void FollowUserSuccessfully(string followedBy,string followerOf)
        {
            var follow = JsonUtility.ToJson(new Follow(followedBy, followerOf));

            var networkService = Substitute.For<INetworkService>();
            var apiResponse = new Subject<string>();


            var command = "followUser";
            networkService.PostJsonData(command, follow).Returns(apiResponse);

            var action = new FollowAction(networkService);
            var followObservable = action.Follow(followedBy, followerOf).Check();
            apiResponse.OnNext(follow);
            apiResponse.OnCompleted();
            
            followObservable.EmittedOnce();
            networkService.Received(1).PostJsonData(command,
                Arg.Is(follow));
        }
    }
}