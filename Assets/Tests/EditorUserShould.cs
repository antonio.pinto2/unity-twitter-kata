using System.Collections;
using Etermax.UnityReactiveX.Testing;
using NSubstitute;
using NUnit.Framework;
using Services.Network;
using UniRx;
using UnityEngine;
using UnityEngine.TestTools;
using Users.Actions;
using Users.Data;

namespace Tests
{
    public class EditUserShould
    {
        [TestCase("realname", "nickname")]
        [TestCase("nickname", "nickname")]
        [TestCase("realname", "")]
        [TestCase("", "nickname")]
        [TestCase("", "")]
        public void EditUserSuccessfully(string realName,string nickName)
        {
            var user = JsonUtility.ToJson(new User(realName, nickName));

            var networkService = Substitute.For<INetworkService>();
            var apiResponse = new Subject<string>();


            var command = "editUser";
            networkService.PostJsonData(command, user).Returns(apiResponse);

            var editUser = new EditUserAction(networkService);
            var registerUserObservable = editUser.EditUser(realName, nickName).Check();
            apiResponse.OnNext(user);
            apiResponse.OnCompleted();
            
            registerUserObservable.EmittedOnce();
            networkService.Received(1).PostJsonData(command,
                Arg.Is(user));
        }
        
        
    }
}