using System;
using Services.Network;
using UnityEngine;
using Users.Data;

namespace Users.Actions
{
    public class GetFollowsAction
    {
        private INetworkService networkService;
        private const string CommandFollowerOf = "getFollowerOf";
        private const string CommandFollowedBy = "getFollowedBy";

        public GetFollowsAction(INetworkService networkService)
        {
            this.networkService = networkService;
        }

        public IObservable<string> GetFollowersOf(string nickname)
        {
            return networkService.PostJsonData(CommandFollowerOf, nickname);
        }
        public IObservable<string> GetFollowedBy(string nickname)
        {
            return networkService.PostJsonData(CommandFollowedBy, nickname);
        }
    }
}