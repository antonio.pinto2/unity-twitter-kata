using System;
using Services.Network;
using UnityEngine;
using Users.Data;

namespace Users.Actions
{
    public class FollowAction
    {
        private INetworkService networkService;
        private const string Command = "followUser";

        public FollowAction(INetworkService networkService)
        {
            this.networkService = networkService;
        }

        public IObservable<string> Follow(string followedBy, string followerOf)
        {
            var followJson = JsonUtility.ToJson(new Follow(followedBy, followerOf));

            return networkService.PostJsonData(Command, followJson);
        }
    }
}