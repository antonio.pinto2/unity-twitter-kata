namespace Users.Data
{
    public class Follow
    {
        public string followedBy, followerOf;
    
        public Follow(string followedBy, string followerOf)
        {
            this.followedBy = followedBy;
            this.followerOf = followerOf;
        }
    }
}
