using System;
using Services.Network;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Users.Actions;

namespace Users.Views
{
    public class FollowView : MonoBehaviour
    {
        public  TMP_InputField followerOfInputField;
        public  TMP_InputField followedByInputField;
        public  TMP_Text errorString;

        public  Button registerButton;
        
        private FollowAction action;

        // Start is called before the first frame update
        void Start()
        {
            action = new FollowAction(new KtorNetworkService());
            registerButton.OnClickAsObservable().Subscribe(_ =>
            {
                action.Follow(followerOfInputField.text, followedByInputField.text)
                    .Subscribe(OnFollowSuccessfully, OnFollowFailed);
            });
        }

        private void OnFollowFailed(Exception exception)
        {
            Debug.LogWarning(exception);
            errorString.SetText(exception.ToString());
        }

        private void OnFollowSuccessfully(string response)
        {
            Debug.Log(response);
            errorString.SetText("");
        }
    }
}