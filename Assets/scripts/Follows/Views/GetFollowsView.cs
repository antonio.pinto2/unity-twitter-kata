using System;
using Menu.Views;
using Services.Network;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Users.Actions;
using Users.Views;

namespace Follows.Views
{
    public class GetFollowsView : MonoBehaviour
    {
        public  TMP_InputField nickNameInputField;
        public  TMP_Text errorString;

        public  Button followerOfButton;
        public  Button followedByButton;
        
        public  TextItemScrollerView scroller;
        
        private GetFollowsAction action;

        // Start is called before the first frame update
        void Start()
        {
            action = new GetFollowsAction(new KtorNetworkService());
            followerOfButton.OnClickAsObservable().Subscribe(_ =>
            {
                action.GetFollowersOf(nickNameInputField.text)
                    .Subscribe(OnGetFollowsSuccessfully, OnGetFollowsFailed);
            });
            followedByButton.OnClickAsObservable().Subscribe(_ =>
            {
                action.GetFollowedBy(nickNameInputField.text)
                    .Subscribe(OnGetFollowsSuccessfully, OnGetFollowsFailed);
            });
        }

        private void OnGetFollowsFailed(Exception exception)
        {
            Debug.LogWarning(exception);
            errorString.SetText(exception.ToString());
        }

        private void OnGetFollowsSuccessfully(string response)
        {
            Debug.Log(response);
            var follows = Tools.JsonUtilityExtension.FromJson<string[]>(response);
            scroller.Clear();
            scroller.Add(follows);
            errorString.SetText("");
        }
    }

    
}