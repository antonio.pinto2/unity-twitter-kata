using System;
using Menu.Views;
using Services.Network;
using TMPro;
using Tweets.actions;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Tweets.Views
{
    public class GetTweetsOfView : MonoBehaviour
    {
        public  TMP_InputField nickNameInputField;
        public  TMP_Text errorString;

        public  Button getTweetsButton;
        
        public  TextItemScrollerView scroller;
        
        private GetTweetsOfAction action;

        // Start is called before the first frame update
        void Start()
        {
            action = new GetTweetsOfAction(new KtorNetworkService());
            getTweetsButton.OnClickAsObservable().Subscribe(_ =>
            {
                action.GetTweetsOf(nickNameInputField.text)
                    .Subscribe(OnGetFollowsSuccessfully, OnGetFollowsFailed);
            });
        }

        private void OnGetFollowsFailed(Exception exception)
        {
            Debug.LogWarning(exception);
            errorString.SetText(exception.ToString());
        }

        private void OnGetFollowsSuccessfully(string response)
        {
            Debug.Log(response);
            var tweets = Tools.JsonUtilityExtension.FromJson<string[]>(response);
            scroller.Clear();
            scroller.Add(tweets);
            errorString.SetText("");
        }
    }

    
}