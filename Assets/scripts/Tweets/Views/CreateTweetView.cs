using System;
using Services.Network;
using TMPro;
using Tweets.actions;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Tweets.Views
{
    public class CreateTweetView : MonoBehaviour
    {
        public  TMP_InputField nickName;
        public  TMP_InputField text;
        public  TMP_Text errorString;

        public  Button submitButton;
        
        private CreateTweetAction action;

        // Start is called before the first frame update
        void Start()
        {
            action = new CreateTweetAction(new KtorNetworkService());
            submitButton.OnClickAsObservable().Subscribe(_ =>
            {
                action.Tweet(nickName.text, text.text)
                    .Subscribe(OnTweetSuccessfully, OnTweetFailed);
            });
        }

        private void OnTweetFailed(Exception exception)
        {
            Debug.LogWarning(exception);
            errorString.SetText(exception.ToString());
        }

        private void OnTweetSuccessfully(string response)
        {
            Debug.Log(response);
            text.text = "";
            errorString.SetText("");
        }
    }
}