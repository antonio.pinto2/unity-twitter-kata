using System;
using Services.Network;
using Tweets.data;
using UnityEngine;

namespace Tweets.actions
{
    public class CreateTweetAction
    {
        private INetworkService networkService;
        private const string Command = "createTweet";

        public CreateTweetAction(INetworkService networkService)
        {
            this.networkService = networkService;
        }

        public IObservable<string> Tweet(string nickName, string text)
        {
            var followJson = JsonUtility.ToJson(new Tweet(nickName, text));

            return networkService.PostJsonData(Command, followJson);
        }
    }
}