using System;
using Services.Network;
using Tweets.data;
using UnityEngine;

namespace Tweets.actions
{
    public class GetTweetsOfAction
    {
        private INetworkService networkService;
        private const string Command = "getTweetsOf";

        public GetTweetsOfAction(INetworkService networkService)
        {
            this.networkService = networkService;
        }

        public IObservable<string> GetTweetsOf(string nickName)
        {
            return networkService.PostJsonData(Command, nickName);
        }
    }
}