namespace Tweets.data
{
    public class Tweet
    {
        public string nickName, text;
    
        public Tweet(string nickName, string text)
        {
            this.nickName = nickName;
            this.text = text;
        }
    }
}
