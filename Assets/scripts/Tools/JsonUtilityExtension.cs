using UnityEngine;

namespace Tools
{
    public static class JsonUtilityExtension
    {
        private class JsonWrapper<T>
        {
            public T items;
        }

        public static T FromJson<T>(string json)
        {
            JsonWrapper<T> jsonWrapper = JsonUtility.FromJson<JsonWrapper<T>>("{\"items\":" + json + "}");
            return jsonWrapper.items;
        }
    }
}