using System;
using System.Collections.Generic;
using UniRx;

namespace Services.Network
{
    public class KtorNetworkService : INetworkService
    {
        private string serverAddress = "http://0.0.0.0:8080/";

        public IObservable<string> PostJsonData(string command, String jsonData)
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            return ObservableWWW.Post(serverAddress + command, 
                    System.Text.Encoding.UTF8.GetBytes(jsonData), 
                    headers);
        }
    }
}