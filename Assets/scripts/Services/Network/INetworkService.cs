using System;

namespace Services.Network
{
    public interface INetworkService
    {
        public IObservable<string> PostJsonData(string command, String jsonData);
    }
}