using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Views
{
    public class MenuView : MonoBehaviour
    {
        public List<MenuViewButtonMenuMap> buttonMenuMap;
        // Start is called before the first frame update
        void Start()
        {
            foreach (var buttonMenu in buttonMenuMap)
            {
                buttonMenu.button.OnClickAsObservable().Subscribe(_ => { ShowMenu(buttonMenu); });
            }
        
        }

        private void ShowMenu(MenuViewButtonMenuMap visibleButtonMenu)
        {
            foreach (var buttonMenu in buttonMenuMap)
            {
                buttonMenu.menu.SetActive(false);
            }
            visibleButtonMenu.menu.SetActive(true);
        }
    }

    [System.Serializable]
    public class MenuViewButtonMenuMap
    {
        public Button button;
        public GameObject menu;
    }
}