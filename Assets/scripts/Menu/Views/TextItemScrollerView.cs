using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Views
{
    [RequireComponent(typeof(ScrollRect))]
    public class TextItemScrollerView: MonoBehaviour
    {
        private List<GameObject> items;
        private ScrollRect scrollView;

        public GameObject itemPrefab;

        private void Start()
        {
            items = new List<GameObject>();
            scrollView = GetComponent<ScrollRect>();
        }

        public void Add(string[] follows)
        {
            foreach (var follow in follows)
            {
                //TODO make entry its own object 
                var entry = GetItem();
                var text = entry.GetComponentInChildren<TMP_Text>();
                if (text) text.text = follow;
            }
        }

        private GameObject GetItem()
        {
            foreach (var it in items)
                if (!it.activeInHierarchy)
                {
                    it.SetActive(true);
                    return it;
                }
            var item = Instantiate(itemPrefab, scrollView.content);
            items.Add(item);
            return item;
        }

        public void Clear()
        {
            foreach (var item in items)
                item.SetActive(false);
        }
    }
}