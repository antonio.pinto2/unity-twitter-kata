using System;
using Services.Network;
using UnityEngine;
using Users.Data;

namespace Users.Actions
{
    public class RegisterUserAction
    {
        private INetworkService networkService;
        private const string Command = "registerUser";
        public RegisterUserAction(INetworkService networkService)
        {
            this.networkService = networkService;
        }

        public IObservable<string> RegisterUser(string realName, string nickName)
        {
            var userJson = JsonUtility.ToJson(new User(realName, nickName));

            return networkService.PostJsonData(Command, userJson);
        }
    }
}