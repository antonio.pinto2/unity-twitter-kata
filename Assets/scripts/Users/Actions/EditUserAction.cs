using System;
using Services.Network;
using UnityEngine;
using Users.Data;

namespace Users.Actions
{
    public class EditUserAction
    {
        private INetworkService networkService;
        private const string Command = "editUser";

        public EditUserAction(INetworkService networkService)
        {
            this.networkService = networkService;
        }

        public IObservable<string> EditUser(string realName, string nickName)
        {
            var userJson = JsonUtility.ToJson(new User(realName, nickName));

            return networkService.PostJsonData(Command, userJson);
        }
    }
}