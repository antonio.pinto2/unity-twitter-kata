using System;
using Services.Network;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Users.Actions;

namespace Users.Views
{
    public class EditUserView : MonoBehaviour
    {
        public  TMP_InputField realNameInputField;
        public  TMP_InputField nickNameInputField;
        public  TMP_Text errorString;

        public  Button registerButton;
        
        private EditUserAction action;

        // Start is called before the first frame update
        void Start()
        {
            action = new EditUserAction(new KtorNetworkService());
            registerButton.OnClickAsObservable().Subscribe(_ =>
            {
                action.EditUser(realNameInputField.text, nickNameInputField.text)
                    .Subscribe(OnUserEditSuccessfully, OnUserEditFailed);
            });
        }

        private void OnUserEditFailed(Exception exception)
        {
            Debug.LogWarning(exception);
            errorString.SetText(exception.ToString());
        }

        private void OnUserEditSuccessfully(string response)
        {
            Debug.Log(response);
            errorString.SetText("");
        }
    }
}