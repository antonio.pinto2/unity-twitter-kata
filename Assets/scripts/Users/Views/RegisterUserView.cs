using System;
using Services.Network;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Users.Actions;

namespace Users.Views
{
    public class RegisterUserView : MonoBehaviour
    {
        public  TMP_InputField realNameInputField;
        public  TMP_InputField nickNameInputField;
        public  TMP_Text errorString;

        public  Button registerButton;
        
        private RegisterUserAction action;

        // Start is called before the first frame update
        void Start()
        {
            action = new RegisterUserAction(new KtorNetworkService());
            registerButton.OnClickAsObservable().Subscribe(_ =>
            {
                action.RegisterUser(realNameInputField.text, nickNameInputField.text)
                    .Subscribe(OnUserSuccessfullyRegister, OnUserFailedRegister);
            });
        }

        private void OnUserFailedRegister(Exception exception)
        {
            Debug.LogWarning(exception);
            errorString.SetText(exception.ToString());
        }

        private void OnUserSuccessfullyRegister(string response)
        {
            Debug.Log(response);
            errorString.SetText("");
        }
    }
}