namespace Users.Data
{
    public class User
    {
        public string realName, nickName;
    
        public User(string realName, string nickName)
        {
            this.realName = realName;
            this.nickName = nickName;
        }
    }
}
