[![Name Badge][name]][name-link]
[![Version Badge][version]][version-link]

[![Pipeline Badge][pipeline]][pipeline-link]
[![Open Issues Badge][open-issues]][open-issues-link]
[![Open Merge Requests Badge][open-merge-requests]][open-merge-requests-link]

[![Slack Badge][slack]][slack-link]

[name]: https://badges.etermax.com/packageJson?key=name&label=name&color=yellow&project=unity-tools/extension-methods/observable-extensions
[name-link]: package.json
[version]: https://badges.etermax.com/packageJson?key=version&label=version&project=unity-tools/extension-methods/observable-extensions
[version-link]: package.json
[pipeline]: https://badges.etermax.com/pipeline?project=unity-tools/extension-methods/observable-extensions
[pipeline-link]: https://gitlab.etermax.net/unity-tools/extension-methods/observable-extensions/pipelines
[open-issues]: https://badges.etermax.com/open-issues?project=unity-tools/extension-methods/observable-extensions
[open-issues-link]: https://gitlab.etermax.net/unity-tools/extension-methods/observable-extensions/issues?scope=all&utf8=✓&state=opened
[open-merge-requests]: https://badges.etermax.com/open-merge-requests?project=unity-tools/extension-methods/observable-extensions
[open-merge-requests-link]: https://gitlab.etermax.net/unity-tools/extension-methods/observable-extensions/merge_requests
[slack]: https://badges.etermax.com/slack?channel=ask-platform-team
[slack-link]: https://etermax.slack.com/app_redirect?channel=ask-platform-team

# Observable Extensions

## Description

This package provides extensions for UniRx library.

# Usage

## Test

- AssertValue

```csharp
var message = "Hello World";
Observable
    .Return(message)
    .Test()
    .AssertValue(message);
```

```csharp
var message = "Hello World";
Observable
    .Return(message)
    .Test()
    .AssertValue(value => value == message);
```

- AssertComplete

```csharp
Observable
    .ReturnUnit()
    .Test()
    .AssertComplete();
```

- AssertNotCompleted

```csharp
Observable
    .Throw<string>(new Exception())
    .Test()
    .AssertNotCompleted();
```

- AssertException

```csharp
var error = new WebException();
Observable
    .Throw<string>(error)
    .Test()
    .AssertException(error);
```

- AssertError

```csharp
Observable
    .Throw<string>(new WebException())
    .Test()
    .AssertError<WebException>();
```

- AssertNoErrors

```csharp
Observable
    .ReturnUnit()
    .Test()
    .AssertNoErrors();
```

- AssertNoOnNext

```csharp
Observable
    .Empty<string>()
    .Test()
    .AssertNoOnNext();
```

## FromAction

Will execute the block and will return Unit to the next Observable. If any error is throw inside the block it will be propagated to the stream.

```csharp
ObservableExtensions
    .FromAction(() => DoSomething());
```

## FromFunction

Will execute the block and forward the value to the next Observable. If any error is throw inside the block it will be propagated to the stream.

```csharp
ObservableExtensions.FromFunction(() => 5)
```

## SelectWhere

Example:

If entry value is type of `Event` will transform to `SomeEvent`

```csharp
var eventMessage = "Hello World!";
Event myEvent = new SomeEvent(eventMessage);
Observable
    .Return(myEvent)
    .SelectWhere<Event, SomeEvent>()
    .Subscribe(@event => Debug.Log(@event.Message));

// Output: "Hello World!"
```

## SwitchIfError

```csharp
var message = "Hello World!";
Observable
    .Throw<string>(new Exception())
    .SwitchIfError(() => Observable.Return(message))
    .Subscribe(value => Debug.Log(value))

// Output: "Hello World!"
```

## OnErrorSwitchWhen

```csharp
var message = "Hello World!";
Observable
    .Throw<string>(new TimeoutException())
    .OnErrorSwitchWhen(e => e is TimeoutException, () => Observable.Return(message))
    .Subscribe(value => Debug.Log(value));

// Output: "Hello World!"
```

## SwitchIfEmpty

```csharp
var message = "Hello World!";
Observable
    .Empty<string>()
    .SwitchIfEmpty(() => Observable.Return(message))
    .Subscribe(value => Debug.Log(message))

// Output: "Hello World!"
```

## ReturnsWithoutWaiting

```csharp
//Given
var expectedData = new Data();
mockCollaborator.GetData().ReturnsWithoutWaiting(expectedData);

//When
testedClass.GetDataFromCollaborator()
    .Subscribe(data => Assert.AreEqual(expectedData, data)); //Then
```

## ThrowsWithoutWaiting

```csharp
//Given
mockCollaborator.GetData().ThrowsWithoutWaiting(new ExpectedException());

//When
testedClass.GetDataFromCollaborator()
    .Subscribe(_ => {},
    error => Assert.IsTrue(error is ExpectedException)); //Then
```

## Dependencies

```
"@etermax/functional": "1.0.1"
"@etermax/unity-rx": "6.2.2"
```
