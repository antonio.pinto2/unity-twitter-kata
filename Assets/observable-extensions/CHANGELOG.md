## 1.2.4

### Added

- ReturnsWithoutWaiting.
- ThrowsWithoutWaiting.
- NeverReturns.
- Completes.

## 1.2.3

### Added

- Assembly Definitions.

## 1.2.2

### Added

- OnErrorSwitchWhen extension method added

## 1.2.1

### Fixed

- AssertValue now uses NSubstitude Arg.Is<T>(predicate) for test validation.

## 1.2.0

### Added

- Tests for every Extension
- TestObservables for Testing Observables

## 1.1.0

### Added

- FromAction and FromFunction methods.

## 1.0.0

- release first version

## 0.0.1

- publish
