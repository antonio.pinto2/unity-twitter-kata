using System;
using UniRx;

namespace Etermax.Observables{
	public static class ObservableExtensions{
		public static IObservable<Unit> FromAction(Action action){
			return Observable.Create<Unit>(emitter => {
				try {
					action.Invoke();
					emitter.OnNext(Unit.Default);
					emitter.OnCompleted();
				}
				catch (Exception exception) {
					emitter.OnError(exception);
				}

				return Disposable.Empty;
			});
		}

		public static IObservable<T> FromFunction<T>(Func<T> function){
			return Observable.Create<T>(emitter => {
				try {
					var value = function.Invoke();
					emitter.OnNext(value);
					emitter.OnCompleted();
				}
				catch (Exception exception) {
					emitter.OnError(exception);	
				}
				
				return Disposable.Empty;
			});
		}

		public static IObservable<TResult> SelectWhere<TSource, TResult>(this IObservable<TSource> source)
			where TResult : TSource{
			return source.Where(@object => @object is TResult).Select(@object => (TResult) @object);
		}

		public static IObservable<TSource>
			ConcatObservables<TSource>(params IObservable<TSource>[] source){
			return source.Concat();
		}

		public static IObservable<T> SwitchIfEmpty<T>(this IObservable<T> source,
			Func<IObservable<T>> other){
			return new SwitchIfEmptyObservable<T>(source, other);
		}

		public static IObservable<T> SwitchIfError<T>(this IObservable<T> source,
			Func<IObservable<T>> other){
			return new SwitchIfErrorObservable<T>(source, other);
		}

		public static IObservable<T> SelectError<T>(this IObservable<T> source,
			Func<Exception, Exception> transform){
			return new SelectErrorObservable<T>(source, transform);
		}
		
		public static IObservable<T> OnErrorSwitchWhen<T>(this IObservable<T> source,
			Func<Exception, bool> predicate, Func<IObservable<T>> other){
			return new OnErrorSwitchIfObservable<T>(source, other, predicate);
		}

		class SwitchIfEmptyObservable<T> : IObservable<T>{
			private readonly IObservable<T> source;
			private readonly Func<IObservable<T>> other;
			private bool hasValue;

			public SwitchIfEmptyObservable(IObservable<T> source, Func<IObservable<T>> other){
				this.source = source;
				this.other = other;
			}

			public IDisposable Subscribe(IObserver<T> observer){
				return source.Subscribe(
					value => {
						hasValue = true;
						observer.OnNext(value);
						observer.OnCompleted();
					}
					, observer.OnError
					, () => {
						if (!hasValue) {
							other.Invoke().Subscribe(observer.OnNext, observer.OnError, observer.OnCompleted);
						}
					});
			}
		}

		class SwitchIfErrorObservable<T> : IObservable<T>{
			private readonly IObservable<T> source;
			private readonly Func<IObservable<T>> other;

			public SwitchIfErrorObservable(IObservable<T> source, Func<IObservable<T>> other){
				this.source = source;
				this.other = other;
			}

			public IDisposable Subscribe(IObserver<T> observer){
				return source.Subscribe(
					value => {
						observer.OnNext(value);
						observer.OnCompleted();
					}
					, exception => other.Invoke().Subscribe(observer.OnNext, observer.OnError, observer.OnCompleted)
					, observer.OnCompleted);
			}
		}

		class SelectErrorObservable<T> : IObservable<T>{
			private readonly IObservable<T> source;
			private readonly Func<Exception, Exception> _transform;

			public SelectErrorObservable(IObservable<T> source, Func<Exception, Exception> transform){
				this.source = source;
				_transform = transform;
			}

			public IDisposable Subscribe(IObserver<T> observer){
				return source.Subscribe(
					observer.OnNext
					, error => observer.OnError(_transform(error))
					, observer.OnCompleted);
			}
		}
		
		class OnErrorSwitchIfObservable<T> : IObservable<T>
		{
			private readonly IObservable<T> source;
			private readonly Func<IObservable<T>> other;
			private readonly Func<Exception, bool> predicate;
			private bool hasValue;

			public OnErrorSwitchIfObservable(IObservable<T> source, Func<IObservable<T>> other,
				Func<Exception, bool> predicate)
			{
				this.source = source;
				this.other = other;
				this.predicate = predicate;
			}

			public IDisposable Subscribe(IObserver<T> observer)
			{
				return source.Subscribe(
					value =>
					{
						observer.OnNext(value);
						observer.OnCompleted();
					}
					, exception =>
					{
						if (predicate(exception))
						{
							other.Invoke().Subscribe(observer.OnNext, observer.OnError, observer.OnCompleted);
						}
						else
						{
							observer.OnError(exception);
						}
					}, observer.OnCompleted);
			}
		}
	}
}