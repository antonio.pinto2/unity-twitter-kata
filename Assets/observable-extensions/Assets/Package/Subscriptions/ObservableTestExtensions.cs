using System;
using System.Linq;
using NSubstitute;
using UniRx;

namespace Etermax.Observables
{
    public static class ObservableTestExtensions
    {
        public static TestSubscription<T> Test<T>(this IObservable<T> observable){
            return new TestSubscription<T>(observable);
        }
        
        public static void ReturnsWithoutWaiting<T>(this IObservable<T> observable, T expected = default(T))
        {
            observable.Returns(Observable.Return(expected));
        }

        public static void ReturnsWithoutWaiting<T>(this IObservable<T> observable, T expected = default(T),
            params T[] returnThese)
        {
            var observables = returnThese.Select(Observable.Return).ToArray();
            observable.Returns(Observable.Return(expected), observables);
        }

        public static void ThrowsWithoutWaiting<T, E>(this IObservable<T> observable, E exception) where E : Exception
        {
            observable.Returns(Observable.Throw<T>(exception));
        }

        public static void NeverReturns<T>(this IObservable<T> observable)
        {
            observable.Returns(Observable.Never<T>());
        }

        public static void Completes<T>(this IObservable<T> observable)
        {
            observable.Returns(Observable.Empty<T>());
        }
    }
}