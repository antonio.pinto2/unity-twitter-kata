The format is based on Keep a Changelog and this project adheres to Semantic Versioning.

## [0.0.3]

### Fix

- Fix compatibility issues with Unity 2018

## [0.0.2]

### Added

- Make the library compatible with Unity 2018.

### Removed

- .NET 3.x test pipeline

## [0.0.1]

### Added

- Source code.
