using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UniRx;
using UnityEngine;
using static System.TimeSpan;
using static UniRx.Observable;

namespace Etermax.UnityReactiveX.Testing
{
	public static class TestObserver
	{
		public static TestObserver<T> Check<T>(this IObservable<T> tested)
		{
			var observer = new TestObserver<T>();
			tested.Subscribe(observer);
			return observer;
		}

		public static ObservableYieldInstruction<object> YieldCheck<T>(
			this IObservable<T> tested,
			Action<TestObserver<T>> checks,
			float timeout = 20
		)
		{
			var observer = new TestObserver<T>();

			return tested
				.Do(observer.OnNext)
				.DoOnError(observer.OnError)
				.DoOnCompleted(observer.OnCompleted)
				.CatchIgnore()
				.TakeUntil(Timer(FromSeconds(timeout)))
				.DoOnTerminate(() => checks(observer))
				.Select(_ => default(object))
				.ToYieldInstruction();
		}
	}

    public class TestObserver<T> : IObserver<T>
    {
	    public IEnumerable<T> Emissions => emittedEvents;
	    
        readonly List<T> emittedEvents = new List<T>();
        
        Exception error;
        bool completed;

        public void OnCompleted() => completed = true;

        public void OnError(Exception emittedError) => error = emittedError;

        public void OnNext(T emittedEvent) => emittedEvents.Add(emittedEvent);

        public TestObserver<T> EmittedOnce(string message = null) =>
	        Assertion(EmittedJustOne(), message ?? $"Expected to emit just one event, but emitted {ShowEvents()}.");

        public TestObserver<T> EmittedOnce<S>(string message = null) =>
	        Assertion(
		        emittedEvents.Any(e => e is S) && EmittedJustOne(),
		        message ?? $"Expected to emit just one [{typeof(S)}] event, but emitted {ShowEventTypes()}."
	        );

        public TestObserver<T> EmittedOnce(T expected, string message = null) =>
	        Assertion(
		        emittedEvents.Contains(expected) && EmittedJustOne(),
		        message ?? $"Expected to emit just one [{expected}] event, but emitted {ShowEvents()}."
		    );

        public TestObserver<T> EmittedOnce(Func<T, bool> condition, string message = null) =>
	        Assertion(
		        emittedEvents.Any(condition) && EmittedJustOne(),
		        message ?? $"Expected to emit just one event matching the condition, but emitted {ShowEvents()}."
		    );

        public TestObserver<T> EmittedLast<S>(string message = null) =>
	        Assertion(
		        emittedEvents.Count > 0,
		        message ?? $"Expected to emit one [{typeof(S)}] event lastly, but no nothing was emitted"
	        ).Assertion(
			    emittedEvents.Last() is S,
		        message ?? $"Expected to emit one [{typeof(S)}] event lastly, but emitted {emittedEvents.Last()}"
	        );

        public TestObserver<T> EmittedLast(T expected, string message = null) =>
	        Assertion(
		        emittedEvents.Count > 0,
		        message ?? $"Expected to emit {expected} lastly, but no nothing was emitted"
	        ).Assertion(
		        emittedEvents.Last().Equals(expected),
		        message ?? $"Expected to emit {expected} lastly, but emitted {emittedEvents.Last()}"
	        );

        public TestObserver<T> EmittedLast(Func<T, bool> condition, string message = null) =>
	        Assertion(
		        emittedEvents.Count > 0,
		        message ?? $"Expected to emit an event matching the condition lastly, but no nothing was emitted"
	        ).Assertion(
		        condition(emittedEvents.Last()),
		        message ?? $"Expected to emit an event matching the condition lastly, but emitted {emittedEvents.Last()}"
	        );

        public TestObserver<T> Emitted(string message = null) =>
	        Assertion(emittedEvents.Any(), message ?? "Expected to emit something, but emitted nothing.");

        public TestObserver<T> Emitted(T expected, string message = null) =>
	        Assertion(
		        emittedEvents.Contains(expected),
		        message ?? $"Expected to emit {expected}, but emitted {ShowEvents()}."
		    );

        public TestObserver<T> Emitted<S>(string message = null) =>
	        Assertion(
		        emittedEvents.Any(e => e is S),
		        message ?? $"Expected to emit a {typeof(S)}, but emitted {ShowEventTypes()}."
	        );

        public TestObserver<T> Emitted(Func<T, bool> condition, string message = null) =>
	        Assertion(
		        emittedEvents.Any(condition),
		        message ?? "Expected any emission matching the condition, but emitted none."
	        );

        public TestObserver<T> EmittedExactly(params T[] values)
        {
	        CollectionAssert.AreEquivalent(values, emittedEvents, $"Expected to emit: {values}, but got {ShowEvents()}");
	        return this;
        }

        public TestObserver<T> DidNotEmit(string message = null) =>
	        Assertion(!emittedEvents.Any(), message ?? $"Expected to emit no events, but events were emitted: {ShowEvents()}.");

        public TestObserver<T> DidNotFail(string message = null) =>
			Assertion(error == null, message ?? $"Expected no error, but got \"{error?.Message}\".");

        public TestObserver<T> Failed(string message = null) =>
	        Assertion(error != null, message ?? "Expected an error, but got none.");
        
        public TestObserver<T> FailedWith<TI>(string message = null) where TI : Exception =>
	        Assertion(error is TI, message ?? $"Expected a {typeof(TI)} error, but got {ShowError(error)}.");

        public TestObserver<T> Completed(string message = null) =>
	        Assertion(completed, message ?? "Expected to complete, but it did not.");

        public TestObserver<T> DidNotComplete(string message = null) =>
	        Assertion(!completed, message ?? "Expected not to complete, but it did.");

        bool EmittedJustOne() => emittedEvents.Count == 1;

        static string ShowError(Exception error) => error != null ? $"a {error.GetType()} error" : "no error";

        TestObserver<T> Assertion(bool condition, string failMessage)
        {
	        if (condition)
				return this;

		    Assert.Fail(failMessage);
		    return this;
        }

        string ShowEvents() => ShowEventsOrNone(emittedEvents);

        string ShowEventTypes() =>
	        ShowEventsOrNone(emittedEvents.Select(e => e.GetType()));

        static string ShowEventsOrNone<S>(IEnumerable<S> events) =>
	        events.Any() ? $"[{string.Join(", ", events)}]" : "no events";
    }
}
