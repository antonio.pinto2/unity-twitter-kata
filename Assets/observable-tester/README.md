# Observable Tester

# Description

A library to write semantic and fluent assertions on observables.

# Prerequisites

.NET 4.x, sorry, no excuses.

# Usage

To test any observable, just use the `.Check()` extension method, then chain your requirements. You can check for a single or multiple emissions, completion, errors, etc.

```csharp
observable
    .Check()
    .DidNotFail()
    .Emitted(Unit.Default)
    .Completed();
```

all test methods after `Check()` can be chained.

## Emit Checks

Check for just for a single, any, or last emission, for an exact list of events in order, or for no emissions at all. Emission check can do so for just the emission, for a specific type, for an exact value, or for a matching condition.

### Emitted

```csharp
observable.Check().Emitted();
observable.Check().Emitted<SomeMessage>();
observable.Check().Emitted(new SomeMessage());
observable.Check().Emitted(emission => Predicate(emission));
```

### Emitted Once

```csharp
observable.Check().EmittedOnce();
observable.Check().EmittedOnce<SomeMessage>();
observable.Check().EmittedOnce(new SomeMessage());
observable.Check().EmittedOnce(emission => Predicate(emission));
```

### Emitted Last

```csharp
observable.Check().EmittedLast(new SomeMessage());
observable.Check().EmittedLast<SomeMesssage>();
observable.Check().EmittedLast(emission => Predicate(emission));
```

### Emitted Exactly

```csharp
observable.Check().EmittedExactly(2, 3, 5, 7, 11);
```

### Did Not Emit

```csharp
observable.Check().DidNotEmit();
```

## Error Checks

Check for any error, for specific errors, or for no errors.

```csharp
observable.Check().Failed();
observalbe.Check().FailedWith<AnError>();
observable.Check().DidNotFail();
```

## Completion Checks

Check for completion or non completion.

```csharp
observable.Check().Completed();
observable.Check().DidNotComplete();
```

## Yielded Checks

It shouldn't be the case when testing observables, but if by any chance you end up testing an observable that will emit asynchronously, you can wait for that emission in a test tagged with `[UnityTest]` by using `YieldCheck`.

Once the tested observable completes, or the timeout time runs out, `YieldCheck` will call its lambda parameter with with an object ready to perform the required tests as if it was created with the `Check()` method.

```csharp
[UnityTest]
public IEnumerator CompleteAfterAWhile()
{
    yield return observable
        .YieldCheck(test => test.Completed(), timeout: 1);
}
```

# Dependencies

```json
"dependencies": {
    "@etermax/unity-rx": "7.x"
},
```

# Contribution

You are free to contribute to this package but please take this [guidelines](https://gitlab.etermax.net/unity-tools/contributing-guideline/blob/master/README.md) to maintain packages quality.
